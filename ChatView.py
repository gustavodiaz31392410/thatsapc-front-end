# -*- coding: utf-8 -*-

from PyQt4 import QtWebKit, QtCore, QtNetwork
from Queue import Queue
import os

ExecPath = os.path.realpath(__file__)
if os.name == "nt":
    ExecPath = ExecPath.replace("\\","/")
ExecPath = ExecPath[:ExecPath.rfind('/')]    

class ChatView(QtWebKit.QWebView):
    
    messageQueue = Queue()
    __loaded = False
    
    def __init__(self,title):
        QtWebKit.QWebView.__init__(self)
        QtWebKit.QWebView.load(self,QtCore.QUrl("file:///"+ExecPath+"/html/chat.html"))
        self.title = title
        self.connect(self,QtCore.SIGNAL("loadFinished(bool)"),self.loaded)
    
    def loaded(self):
        if (self.title): 
            self.setTitle(self.title)
            self.title = None
        while (not self.messageQueue.empty()):
            msgid =   self.messageQueue.get()
            content = self.messageQueue.get()
            self.appendMessage(self,msgid,content)
        self.messageQueue = None
        self.__loaded = True
    
    def appendMessage(self, msgid, content):
        if (not self.__loaded):
            self.messageQueue.put(msgid)
            self.messageQueue.put(content)
            return
        if (content["type"] == "text"): 
            call = "appendMessage('{0}','{1}','text','{2}',null);".format(msgid,content["text"],content["owner-name"])
            self.page().mainFrame().evaluateJavaScript(QtCore.QString(call))
            
    def changeState(self,msgid,state):
        call = "changeState('{0}','{1}');".format(msgid,state)
        self.page().mainFrame().evaluateJavaScript(QtCore.QString(call))
        
    def setTitle(self,title):
        call = "setTitle('{0}');".format(title)
        self.page().mainFrame().evaluateJavaScript(QtCore.QString(call))


    
